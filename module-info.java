module com.javafxcontrols.colorOptions {
    requires transitive javafx.base;
    requires transitive javafx.controls;
    requires transitive javafx.fxml;
    requires transitive javafx.graphics;
    
    exports com.javafxcontrols.colorOptions;
    opens com.javafxcontrols.colorOptions;
}