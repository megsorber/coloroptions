package com.javafxcontrols.colorOptions;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Main extends Application {
	Label l;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			FlowPane root = new FlowPane();
			ColorOptions co = new ColorOptions();
			root.getChildren().add(co);
			
			co.setFillColor(Color.BLUEVIOLET);
			co.setFillColor(Color.CHARTREUSE);
			co.setFillColor(Color.DARKMAGENTA);
			co.setStrokeColor(Color.YELLOW);
			co.setStrokeColor(Color.PEACHPUFF);
			co.setStrokeColor(Color.CHARTREUSE);
			co.setStrokeColor(Color.BLACK);
			
			Rectangle r = new Rectangle(50, 80);
			r.setStrokeWidth(10);
			r.strokeProperty().bind(co.strokeColorProperty());
			r.fillProperty().bind(co.fillColorProperty());
			root.getChildren().add(r);
			
			Scene scene = new Scene(root,600,200);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
