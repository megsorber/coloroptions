package com.javafxcontrols.colorOptions;

import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Ellipse;
import javafx.scene.text.Text;

import java.io.IOException;
import java.util.ArrayList;

import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;

public class ColorOptions extends HBox {

	public SimpleObjectProperty<Color> fillColor = new SimpleObjectProperty<Color>(Color.WHITE);
	public SimpleObjectProperty<Color> strokeColor = new SimpleObjectProperty<Color>(Color.BLACK);

	@FXML private Pane strokeColorPane;
	@FXML private RadioButton strokeColorButton;
	private Ellipse strokeColorEllipse = new Ellipse(38, 32, 23, 23);
	
	@FXML private Pane fillColorPane;
	@FXML private RadioButton fillColorButton;
	private Ellipse fillColorEllipse = new Ellipse(38, 32, 26, 26);
	
	@FXML private Pane customColorPane;
	@FXML private Button customColorButton;
	private Ellipse customColorEllipse = new Ellipse(38, 32, 26, 26);
	@FXML private ToggleGroup fillOrStroke;
	@FXML GridPane colorsGrid;
	@FXML ColorButton selectedColorButton;
	@FXML ColorPicker customColorPicker;
	
	ArrayList<ColorButton> defaultColors = new ArrayList<ColorButton>();
	ArrayList<ColorButton> customColors = new ArrayList<ColorButton>();
	
	public ColorOptions() {
		super();
		initialize();
	}

	public void disablePremium(EventHandler<ActionEvent> premiumMethod){
		customColorButton.setOnAction(premiumMethod);
	}
	
	public void enablePremium(){
		customColorButton.setOnAction(this::customColorButtonClicked);
	}
	
	private void initialize() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ColorOptions.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);

		try {
			fxmlLoader.load();
			strokeColorButton.getStyleClass().remove("radio-button");
			strokeColorButton.setSelected(true);
			fillColorButton.getStyleClass().remove("radio-button");
			customColorButton.getStyleClass().remove("button");
			
			int borderWidth = 6;
			strokeColorEllipse.setStrokeWidth(borderWidth);
			strokeColorEllipse.setFill(Color.WHITE);
			
			Ellipse innerTrace = new Ellipse(38, 32, 23-borderWidth/2, 23-borderWidth/2);
			innerTrace.setFill(Color.TRANSPARENT);
			innerTrace.setStroke(Color.GRAY);
			innerTrace.setStrokeWidth(.5);
			Ellipse outerTrace = new Ellipse(38, 32, 23+borderWidth/2, 23+borderWidth/2);
			outerTrace.setFill(Color.TRANSPARENT);
			outerTrace.setStroke(Color.GRAY);
			outerTrace.setStrokeWidth(.5);
			
			strokeColorPane.getChildren().add(strokeColorEllipse);
			strokeColorPane.getChildren().add(innerTrace);
			strokeColorPane.getChildren().add(outerTrace);
			Text strokeText = new Text(24, 72, "Line\nColor");
			strokeText.getStyleClass().add("text");
			strokeColorPane.getChildren().add(strokeText);

			fillColorEllipse.setStroke(Color.GRAY);
			fillColorEllipse.setStrokeWidth(.5);
			fillColorPane.getChildren().add(fillColorEllipse);
			Text fillText = new Text(24, 72, "Fill\nColor");
			fillText.getStyleClass().add("text");
			fillColorPane.getChildren().add(fillText);
			
			Stop[] stops = new Stop[] {
					new Stop(0, Color.RED), new Stop(1/6.0, Color.ORANGE), 
					new Stop(2/6.0, Color.YELLOW), new Stop(3/6.0, Color.GREEN), 
					new Stop(4/6.0, Color.BLUE), new Stop(5/6.0, Color.INDIGO), 
					new Stop(6/6.0, Color.VIOLET)};
			LinearGradient customColorGradient = new LinearGradient(0, 0, 1, 0, true, CycleMethod.NO_CYCLE, stops);
			customColorEllipse.setFill(customColorGradient);
			customColorEllipse.setRotate(135.0);
			customColorEllipse.setStroke(Color.GRAY);
			customColorEllipse.setStrokeWidth(.5);
			customColorPane.getChildren().add(customColorEllipse);
			Text customColorText = new Text(18, 72, "Custom\nColor");
			customColorText.getStyleClass().add("text");
			customColorPane.getChildren().add(customColorText);
			
			populateColorsGrid();
		} catch (IOException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	private void populateColorsGrid() {
		Color[] colors = {Color.RED, Color.ORANGE, Color.YELLOW,
				Color.GREEN, Color.BLUE, Color.INDIGO, Color.VIOLET};
		for(int i=0; i<colors.length; i++) {
			Color darkColor = colors[i].darker().darker();
			ColorButton dcb = new ColorButton(darkColor);
			colorsGrid.add(dcb, i, 0);
			defaultColors.add(dcb);
			
			Color color = colors[i];
			ColorButton cb = new ColorButton(color);
			colorsGrid.add(cb, i, 1);
			defaultColors.add(cb);
			
			Color lightColor = colors[i].desaturate().desaturate();
			ColorButton lcb = new ColorButton(lightColor);
			colorsGrid.add(lcb, i, 2);
			defaultColors.add(lcb);
			
			ColorButton cb1 = new ColorButton(Color.WHITE);
			colorsGrid.add(cb1, i, 3);
			customColors.add(cb1);
		}
		ColorButton black = new ColorButton(Color.BLACK);
		colorsGrid.add(black, colors.length, 0);
		defaultColors.add(black);
		
		ColorButton gray = new ColorButton(Color.GRAY);
		colorsGrid.add(gray, colors.length, 1);
		defaultColors.add(gray);
		
		ColorButton white = new ColorButton(Color.WHITE);
		colorsGrid.add(white, colors.length, 2);
		defaultColors.add(white);
		
		ColorButton cb1 = new ColorButton(Color.WHITE);
		colorsGrid.add(cb1, colors.length, 3);
		customColors.add(cb1);
		
		for(Node n:colorsGrid.getChildren()) {
			ColorButton cb=(ColorButton)n;
			cb.setOnAction(this::colorSelected);
			cb.getStyleClass().add("color-button");
		}
		setStrokeColor(Color.BLACK);
	}

	public void customColorButtonClicked(ActionEvent e) {
		if(customColorPicker.isShowing()) {
			customColorPicker.hide();
		}else {
			customColorPicker.show();
		}
	}
	
	public void customColorSelected(ActionEvent e) {
		ColorPicker cp = (ColorPicker)e.getTarget();
		updateColor(cp.getValue());
		
		//If the currently selected colorButton is in the custom colors section, replace
		//that color. Otherwise add the custom color to the first available slot, if one
		//is available
		if(customColors.contains(selectedColorButton)) {
			selectedColorButton.setColor(cp.getValue());
		}else {
			addToCustomColors(cp.getValue());
		}
	}
	
	private void addToCustomColors(Color color) {
		for(ColorButton cb:customColors) {
			if(cb.getColor().equals(Color.WHITE)){
				cb.setColor(color);
				break;
			}
		}
	}
	
	private void colorSelected(ActionEvent e) {
		selectedColorButton = (ColorButton) e.getTarget();
		updateColor(selectedColorButton.getColor());
	}
	
	private void updateColor(Color color) {
		if(fillOrStroke.getSelectedToggle().equals(fillColorButton)) {
			fillColor.set(color);
			fillColorEllipse.setFill(color);
		}else {
			strokeColor.set(color);
			strokeColorEllipse.setStroke(color);
		}
	}
	
	public SimpleObjectProperty<Color> strokeColorProperty() {
		return this.strokeColor;
	}
	
	public Color getStrokeColor() {
		return this.strokeColor.get();
	}
	
	public void setStrokeColor(final Color strokeColor) {
		this.strokeColorProperty().set(strokeColor);
		//if the new color is in either the default of custom colors, select it
		//otherwise add it to the custom colors
		ColorButton selectedColorButton = getColorButton(strokeColor);
		if(selectedColorButton!=null) {
		}else {
			addToCustomColors(strokeColor);
		}
		strokeColorEllipse.setStroke(strokeColor);
	}
	
	public SimpleObjectProperty<Color> fillColorProperty() {
		return this.fillColor;
	}
	
	public Color getFillColor() {
		return this.fillColor.get();
	}
	
	public void setFillColor(final Color fillColor) {
		this.fillColorProperty().set(fillColor);
		//if the new color is in either the default of custom colors, select it
		//otherwise add it to the custom colors
		ColorButton selectedColorButton = getColorButton(fillColor);
		if(selectedColorButton!=null) {
		}else {
			addToCustomColors(fillColor);
		}
		fillColorEllipse.setFill(fillColor);
	}
	
	private ColorButton getColorButton(Color color) {
		for(ColorButton cb:this.defaultColors) {
			if(cb.getColor().equals(color))
				return cb;
		}
		for(ColorButton cb:this.customColors) {
			if(cb.getColor().equals(color))
				return cb;
		}
		return null;
	}
	
	private class ColorButton extends Button{
		private Color color;
		private CornerRadii cornerRadii = new CornerRadii(4);
		
		public ColorButton(Color color) {
			super();
			BackgroundFill fill = new BackgroundFill(color, cornerRadii, Insets.EMPTY);
			this.backgroundProperty().set(new Background(fill));
			this.color = color;
		}

		public Color getColor() {
			return color;
		}

		public void setColor(Color color) {
			BackgroundFill fill = new BackgroundFill(color, cornerRadii, Insets.EMPTY);
			this.backgroundProperty().set(new Background(fill));
			this.color = color;
		}
	}
}
